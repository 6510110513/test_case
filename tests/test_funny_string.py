from code_prog.funny_string import funnyString
import unittest

class funny_string_test(unittest.TestCase):
    def test_abc(self):
        string = "abc"
        answer = "Funny"
        wrong = "incorrect"
        output = funnyString(string)
        self.assertEqual(output,answer,wrong)

    def test_abcC(self):
        string = "bcxz"
        answer = "Not Funny"
        wrong = "incorrect"
        output = funnyString(string)
        self.assertEqual(output,answer,wrong)