from code_prog.fizz_buzz import fizz_buzz
import unittest

class fizz_buzz_test(unittest.TestCase):
    def test_Buzz(self):
        num = 5
        answer = "Buzz"
        result = fizz_buzz(num)
        self.assertEqual(result,answer)

    def test_Fiizz(self):
        num = 3
        answer = "Fizz"
        result = fizz_buzz(num)
        self.assertEqual(result,answer)

    def test_Fizz_Buzz(self):
        num = 15
        answer = "FizzBuzz"
        result = fizz_buzz(num)
        self.assertEqual(result,answer)