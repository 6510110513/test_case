from code_prog.alternating_characters import alternatingCharacters
import unittest

class alternating_characters_test(unittest.TestCase):
    def test_AAAA(self):
        string = "AAAA"
        answer = 3
        result = alternatingCharacters(string)
        self.assertEqual(result,answer)