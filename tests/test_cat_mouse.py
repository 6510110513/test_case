from code_prog.cat_mouse import cat_mouse
import unittest

class cat_mouse_test(unittest.TestCase):
    def test_catA(self):
        x,y,z = 1,6,2
        answer = "cat A"
        result = cat_mouse(x,y,z)
        self.assertEqual(result,answer)
    
    def test_catB(self):
        x,y,z = 1,6,5
        answer = "cat B"
        result = cat_mouse(x,y,z)
        self.assertEqual(result,answer)

    def test_mouseC(self):
        x,y,z = 1,3,2
        answer = "mouse C"
        result = cat_mouse(x,y,z)
        self.assertEqual(result,answer)