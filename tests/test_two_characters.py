from code_prog.two_characters import alternate
import unittest

class two_characters(unittest.TestCase):
    def test_abc(self):
        string = "beabeefeab"
        answer = 5
        wrong = "incorrect"
        output = alternate(string)
        self.assertEqual(output,answer,wrong)