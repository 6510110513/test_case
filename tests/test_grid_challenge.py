from code_prog.grid_challenge import gridChallenge
import unittest

class grid_challenge_test1(unittest.TestCase):
    def test_abc(self):
        grid = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        answer = "YES"
        wrong = "incorrect"
        output = gridChallenge(grid)
        self.assertEqual(output,answer,wrong)

    def test_abcc(self):
        string = ["mpxz","abcd","wlmf"]
        answer = "NO"
        wrong = "incorrect"
        output = gridChallenge(string)
        self.assertEqual(output,answer,wrong)