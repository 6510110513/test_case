from code_prog.caesar_cipher import caesarCipher
import unittest

class caesar_cipher_test(unittest.TestCase):
    def test_up2(self):
        string = "middle-Outz"
        shif = 2
        answer = "okffng-Qwvb"
        output = caesarCipher(string,shif)
        self.assertEqual(output,answer)
