def funnyString(s):
    a = [abs(ord(s[i]) - ord(s[i+1])) for i in range(len(s)-1)]
    if a[::-1] == a: return "Funny"
    else:return "Not Funny"