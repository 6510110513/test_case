def caesarCipher(s, k):
    # Write your code here
    k = k % 26
    a = "abcdefghijklmnopqrstuvwxyz"
    aa = a.upper()
    b = a[k:]+a[:k]
    bb = aa[k:]+aa[:k]
    ss = ''
    for i in range(len(s)):
        if s[i] in a:
            ss += b[a.index(s[i])]
        elif s[i] in aa:
            ss += bb[aa.index(s[i])]
        else: ss += s[i]
    return ss
