def alternatingCharacters(s):
    p = 0
    for i in range(len(s)-1):
        if s[i] == s[i+1]: p+=1
    return p